package com.example.maps.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.maps.MainActivity;
import com.example.maps.R;
import com.example.maps.fragments.CompassFragment;
import com.example.maps.fragments.LocationFragment;
import com.example.maps.interfaces.ReplaceFragment;
import com.example.maps.model.GridModel;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    private ArrayList<GridModel> arrayList;
    private Context context;
    private ReplaceFragment replaceFragment;

    public GridAdapter(ArrayList<GridModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {


        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // get layout from mobile.xml
            assert inflater != null;
            view = inflater.inflate(R.layout.item_grid_view, null);
        }
        ImageView gridImage = view.findViewById(R.id.gridImage);
        TextView gridName = view.findViewById(R.id.gridName);
        gridImage.setImageResource(arrayList.get(i).getImage());
        gridName.setText(arrayList.get(i).getName());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (i) {
                    case 0:
                        RouteFinder();
                        break;
                    case 1:
                        ((MainActivity) context).ReplaceFrag(new LocationFragment(), "Location", true);
                        break;
                    case 2:
                        Explore();
                        break;
                    case 3:
                        NearBy();
                        break;
                    case 5:
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("isShare", true);
                        LocationFragment fragment = new LocationFragment();
                        fragment.setArguments(bundle);
                        ((MainActivity) context).ReplaceFrag(fragment, "Location", true);
                        break;

                    case 6:
                        Street();
                        break;

                    case 8:
                        ((MainActivity) context).ReplaceFrag(new CompassFragment(), "compass", true);
                        break;
                    case 9:
                        Earth();
                        break;

                }

            }
        });
        return view;
    }

    private void RouteFinder() {
        //  String strUri = "google.navigation:q=";
        String strUri = "https://maps.googleapis.com/maps/api/staticmap?center=Australia&key=AIzaSyD8BTPKFnrsdFvfJXayd9CRG5g_fWM_LTQ";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else
            Toast.makeText(context, "Install Google Maps App", Toast.LENGTH_SHORT).show();
    }

    private void NearBy() {
        Uri gmmIntentUri = Uri.parse("geo:0,0?z=10&q=restaurants");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(mapIntent);
        } else
            Toast.makeText(context, "Install Google Maps App", Toast.LENGTH_SHORT).show();
    }

    private void Explore() {
        Uri gmmIntentUri = Uri.parse("geo:0,0?z=10&q=");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(mapIntent);
        } else
            Toast.makeText(context, "Install Google Maps App", Toast.LENGTH_SHORT).show();
    }

    private void Earth() {

        Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.google.earth");
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
        }
        else
            Toast.makeText(context, "Install Google Earth App", Toast.LENGTH_SHORT).show();

    }

    private void Street() {

        Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.google.android.street");
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
        }
        else
            Toast.makeText(context, "Install Google Street View App", Toast.LENGTH_SHORT).show();

    }
}
