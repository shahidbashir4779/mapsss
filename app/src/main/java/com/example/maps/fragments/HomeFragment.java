package com.example.maps.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.maps.MainActivity;
import com.example.maps.R;
import com.example.maps.adapter.GridAdapter;
import com.example.maps.interfaces.ReplaceFragment;
import com.example.maps.model.GridModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ReplaceFragment {


    private Context context;
    private View view;
    private GridView gridView;
    private ArrayList<GridModel> arrayList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null)
            view = inflater.inflate(R.layout.fragment_home, container, false);
        init();
        return view;
    }

    private void init() {
        gridView = view.findViewById(R.id.gridView);
        if (arrayList.size()<1)
            AddData();

        GridAdapter adapter = new GridAdapter(arrayList, context);
        gridView.setAdapter(adapter);


    }

    private void AddData() {
        arrayList.add(new GridModel("Find Route", R.drawable.ic_route_finder));
        arrayList.add(new GridModel("Live Traffic", R.drawable.ic_live_traffic));
        arrayList.add(new GridModel("My Location", R.drawable.ic_my_loction));
        arrayList.add(new GridModel("Near By Places", R.drawable.ic_near_palces));
        arrayList.add(new GridModel("Find Address", R.drawable.ic_find_address));
        arrayList.add(new GridModel("Share Location", R.drawable.ic_share_loc));
        arrayList.add(new GridModel("Street View", R.drawable.ic_street_view));
        arrayList.add(new GridModel("Satellite", R.drawable.ic_statlite));
        arrayList.add(new GridModel("Compass", R.drawable.icon_compass));
        arrayList.add(new GridModel("World Map", R.drawable.icon_earth));

    }

    @Override
    public void replaceFrag(Fragment fragment) {

    }
}
